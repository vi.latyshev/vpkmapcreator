﻿using System;
using System.Linq;

using System.IO;
using IniParser;
using IniParser.Model;
using System.Reflection;
using System.Threading;
using System.Diagnostics;
using System.IO.Compression;
using SteamDatabase.ValvePak;

namespace VpkMapCreator
{
    class Program
    {
        static DirectoryInfo Source;

        static FileIniDataParser ConfigIniData = new FileIniDataParser();
        static IniData ConfigData;

        static readonly string configName = "config.ini";

        static string folderVPKCreator = "VpkCreator";

        static void Main()
        {

            GetConfig();
            
            if (!CheckDirectory(ConfigData["Directory"]["Source"], true))
            {

                if (ConfigData["Directory"]["Source"] == "Source")
                {
                    Directory.CreateDirectory("Source");
                    Console.WriteLine("Drop source files (vpk) to 'Source' folder and press Enter");
                    Console.ReadLine();
                    Main();
                    return;
                }
                else
                {
                    Console.WriteLine("Folder {0} is not found", ConfigData["Directory"]["Source"]);
                    Console.ReadLine();
                    return;
                }
            }

            if (CheckDirectory(ConfigData["Directory"]["Output"]))
            {
                Console.WriteLine("Deleting OLD files...");
                Directory.Delete(ConfigData["Directory"]["Output"], true);
            }

            Directory.CreateDirectory(ConfigData["Directory"]["Output"]);

            Source = new DirectoryInfo(ConfigData["Directory"]["Source"]);

            Source.GetDirectories().All(directory => {
                directory.Delete(true);
                return true;
            });

            Console.WriteLine("\nExtracting from VPK..");

            var SourceFiles = Source.GetFiles();

            for (var i = 0; i < SourceFiles.Length; i++)
            {
                Console.Write("\t- " + SourceFiles[i].Name);

                var Current = Directory.GetCurrentDirectory();

                Directory.SetCurrentDirectory(ConfigData["Directory"]["Source"]);

                ExtractingVpk(
                    SourceFiles[i].FullName,
                    Path.GetFileNameWithoutExtension(SourceFiles[i].Name)
                );

                Directory.SetCurrentDirectory(Current);

                Console.WriteLine(" - done");
            }

            if (bool.Parse(ConfigData["Archive"]["VpkCreate"]))
            {
                if (!Directory.Exists(folderVPKCreator) || !File.Exists("vpk.exe"))
                {
                    ExtractVPKCreator();
                }

                StandartCombinating();

            } else OnlySourceCombinating();

            Console.WriteLine("\n...done");

            Console.ReadLine();
        }

        #region Utils

        public static string FirstCharToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("Input is Null or Empty");
            return input.First().ToString().ToUpper() + input.Substring(1);
        }

        static bool CheckDirectory(string directory, bool checkIn = false)
        {
            if (!Directory.Exists(directory))
                return false;

            if (checkIn)
                return Directory.EnumerateFiles(directory, "*.*", SearchOption.AllDirectories).Count() != 0;

            return true;
        }

        static void GetConfig()
        {
            if (!File.Exists(configName))
            {
                File.WriteAllText(configName, "[Default]");

                ConfigData = ConfigIniData.ReadFile(configName);

                ConfigData.Sections.RemoveSection("Default");

                ConfigData.Sections.AddSection("Directory");

                ConfigData["Directory"].AddKey("Source", "Source");
                ConfigData["Directory"].AddKey("Output", "Output");

                ConfigData["Directory"].AddKey("MainMap", "dota");

                ConfigData.Sections.AddSection("Archive");

                ConfigData["Archive"].AddKey("VpkCreate", "false");
                ConfigData["Archive"].AddKey("ZipCreate", "false");

                ConfigData.Sections.GetSectionData("Archive").Keys.GetKeyData("ZipCreate").Comments.Add("Creating ZIP archive only if 'VpkCreate = true'");

                ConfigIniData.WriteFile(configName, ConfigData);

                Console.WriteLine("Config created. Change it if you need and press Enter to continue");

                Console.ReadLine();

                try
                {
                    Process.Start(Assembly.GetExecutingAssembly().Location);
                    Process.GetCurrentProcess().Kill();
                }
                catch
                { }

            }
            else ConfigData = ConfigIniData.ReadFile(configName);
        }

        static bool CopyFolderContents(string SourcePath, string DestinationPath)
        {
            SourcePath = SourcePath.EndsWith(@"\") ? SourcePath : SourcePath + @"\";
            DestinationPath = DestinationPath.EndsWith(@"\") ? DestinationPath : DestinationPath + @"\";

            try
            {
                if (Directory.Exists(SourcePath))
                {
                    if (Directory.Exists(DestinationPath) == false)
                    {
                        Directory.CreateDirectory(DestinationPath);
                    }

                    foreach (string files in Directory.GetFiles(SourcePath))
                    {
                        FileInfo fileInfo = new FileInfo(files);
                        fileInfo.CopyTo(string.Format(@"{0}\{1}", DestinationPath, fileInfo.Name), true);
                    }

                    foreach (string drs in Directory.GetDirectories(SourcePath))
                    {
                        DirectoryInfo directoryInfo = new DirectoryInfo(drs);
                        if (CopyFolderContents(drs, DestinationPath + directoryInfo.Name) == false)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Combinating

        static void OnlySourceCombinating()
        {
            Console.WriteLine("\nStarting combination files... ONLY SOURCE");

            var SourceDirs = Source.GetDirectories();

            for (var i = 0; i < SourceDirs.Length; i++)
            {
                Console.Write("\t- " + SourceDirs[i].Name);

                if (!CheckDirectory(ConfigData["Directory"]["Output"] + "\\" + SourceDirs[i].Name))
                    Directory.CreateDirectory(ConfigData["Directory"]["Output"] + "\\" + SourceDirs[i].Name);

                if (ConfigData["Directory"]["MainMap"] == SourceDirs[i].Name)
                {
                    CopyFolderContents(
                        ConfigData["Directory"]["Source"] + "\\" + SourceDirs[i].Name,
                        ConfigData["Directory"]["Output"] + "\\" + SourceDirs[i].Name
                    );
                }
                else
                {
                    CopyFolderContents(
                        ConfigData["Directory"]["Source"] + "\\" + ConfigData["Directory"]["MainMap"],
                        ConfigData["Directory"]["Output"] + "\\" + SourceDirs[i].Name
                    );

                    CopyFolderContents(
                        ConfigData["Directory"]["Source"] + "\\" + SourceDirs[i].Name,
                        ConfigData["Directory"]["Output"] + "\\" + SourceDirs[i].Name
                    );

                    File.Delete(ConfigData["Directory"]["Output"] + "\\" + SourceDirs[i].Name + "\\maps\\dota.vmap_c");
                    File.Move(
                        ConfigData["Directory"]["Output"] + "\\" + SourceDirs[i].Name + "\\maps\\" + SourceDirs[i].Name.ToLower() + ".vmap_c",
                        ConfigData["Directory"]["Output"] + "\\" + SourceDirs[i].Name + "\\maps\\dota.vmap_c"
                    );
                }
                Console.WriteLine(" - done");
            };
        }

        static void StandartCombinating()
        {

            Console.WriteLine("\nStarting Combinating and VPK creating..");

            var SourceDirs = Source.GetDirectories();

            for (var i = 0; i < SourceDirs.Length; i++)
            {
                Console.Write("\t- " + SourceDirs[i].Name);

                if (CheckDirectory(folderVPKCreator + "\\dota"))
                    Directory.Delete(folderVPKCreator + "\\dota", true);

                Directory.CreateDirectory(folderVPKCreator + "\\dota");

                if (ConfigData["Directory"]["MainMap"] == SourceDirs[i].Name)
                {
                    CopyFolderContents(
                        ConfigData["Directory"]["Source"] + "\\" + SourceDirs[i].Name,
                        folderVPKCreator + "\\dota"
                    );
                }
                else
                {
                    CopyFolderContents(
                        ConfigData["Directory"]["Source"] + "\\" + ConfigData["Directory"]["MainMap"],
                        folderVPKCreator + "\\dota"
                    );

                    CopyFolderContents(
                        ConfigData["Directory"]["Source"] + "\\" + SourceDirs[i].Name,
                        folderVPKCreator + "\\dota"
                    );

                    File.Delete(folderVPKCreator + "\\dota\\maps\\dota.vmap_c");
                    File.Move(
                        folderVPKCreator + "\\dota\\maps\\" + SourceDirs[i].Name.ToLower() + ".vmap_c",
                        folderVPKCreator + "\\dota\\maps\\dota.vmap_c"
                    );
                }

                CreateVPK();


                var Name = SourceDirs[i].Name.Replace("dota", "");

                Name = Name == string.Empty ? "Default" : FirstCharToUpper(Name.Replace("_", ""));

                if (CheckDirectory(ConfigData["Directory"]["Output"] + "\\" + Name))
                    Directory.Delete(ConfigData["Directory"]["Output"] + "\\" + Name, true);
                Directory.CreateDirectory(ConfigData["Directory"]["Output"] + "\\" + Name);


                File.Move(
                    folderVPKCreator + "\\dota.vpk",
                    ConfigData["Directory"]["Output"] + "\\" + Name + "\\dota.vpk"
                );

                if (bool.Parse(ConfigData["Archive"]["ZipCreate"]))
                {
                    ZipFile.CreateFromDirectory(
                        ConfigData["Directory"]["Output"] + "\\" + Name,
                        ConfigData["Directory"]["Output"] + "\\" + Name + ".zip"
                    );
                    /*
                    File.Move(
                        ConfigData["Directory"]["Output"] + "\\" + Name + ".zip",
                        ConfigData["Directory"]["Output"] + "\\" + Name + "\\" + Name + ".zip"
                    );
                    */
                }

                Console.WriteLine(" - done");
            }
        }

        #endregion

        #region Vpk

        static void ExtractVPKCreator()
        {
            if (!Directory.Exists(folderVPKCreator))
            {
                Directory.CreateDirectory(folderVPKCreator);
            }

            File.WriteAllBytes(Path.Combine(folderVPKCreator, "vpk.exe"), Properties.Resources.vpk);
            File.WriteAllBytes(Path.Combine(folderVPKCreator, "filesystem_stdio.dll"), Properties.Resources.filesystem_stdio);
            File.WriteAllBytes(Path.Combine(folderVPKCreator, "tier0.dll"), Properties.Resources.tier0);
            File.WriteAllBytes(Path.Combine(folderVPKCreator, "tier0_s.dll"), Properties.Resources.tier0_s);
            File.WriteAllBytes(Path.Combine(folderVPKCreator, "vstdlib.dll"), Properties.Resources.vstdlib);
            File.WriteAllBytes(Path.Combine(folderVPKCreator, "vstdlib_s.dll"), Properties.Resources.vstdlib_s);
        }

        static void WaitVpk()
        {
            while (true)
            {
                if (Process.GetProcessesByName("vpk").Count() > 0)
                    Thread.Sleep(100);
                else break;
            }

        }

        static void CreateVPK()
        {
            var Current = Directory.GetCurrentDirectory();

            Directory.SetCurrentDirectory(folderVPKCreator);

            if (!Directory.Exists("dota"))
                Directory.CreateDirectory("dota");

            Process.Start("vpk.exe", "dota");

            WaitVpk();

            if (!File.Exists("dota.vpk"))
            {
                if (File.Exists("dota.bat"))
                    File.Delete("dota.bat");

                File.WriteAllText("vpk.bat", "vpk.exe dota");

                Process.Start("vpk.bat");

                WaitVpk();
            }

            Directory.SetCurrentDirectory(Current);
        }

        static void ExtractingVpk(string vpkName, string Output)
        {
            var package = new Package();

            try
            {
                package.Read(vpkName);
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(e);
                Console.ResetColor();
            }

            foreach (var type in package.Entries)
            {

                var entries = package.Entries[type.Key];

                foreach (var file in entries)
                {

                    var filePath = string.Format("{0}.{1}", file.FileName, file.TypeName);

                    if (!string.IsNullOrWhiteSpace(file.DirectoryName))
                    {
                        filePath = Path.Combine(file.DirectoryName, filePath);
                    }

                    filePath = filePath.Replace('\\', '/');

                    if (Path.DirectorySeparatorChar != '/')
                    {
                        filePath = filePath.Replace('/', Path.DirectorySeparatorChar);
                    }

                    byte[] output;
                    package.ReadEntry(file, out output);

                    var outputFile = Path.Combine(Output, filePath);

                    Directory.CreateDirectory(Path.GetDirectoryName(outputFile));

                    File.WriteAllBytes(outputFile, output);
                }
            }

        }

        #endregion
    }
}
