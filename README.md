###### Welcome from 2017! My old source, don't facepalm

# VPK Map Creator

### Auto creating terrains from .vpk files of Dota 2

##### vpk.exe and its dependencies included

## Installing

* Install [.NET Framework 4.5](https://www.microsoft.com/en-us/download/details.aspx?id=30653)
* Download [VpkMapCreator.zip](../-/releases) from last releases

## Usage

* After the first launch will be created `config.ini` file and `VpkCreator` folder with `vpk.exe`

Drop dota.vpk and some source files (vpk) to 'Source' (or folder from ``config.ini``) folder.

## config.ini

##### [Directory]
* `Source`: string, default `Source`
    * source directory with all .vpk files
* `Output`: string, default `Output`
	* directory with combination and output .vpk files
* `MainMap`: string, default: `dota`
	* main .vpk files for combination another files

##### [Archive]
* `VpkCreate`: boolean, default false
	* creating combinated source files with main .vpk
* `ZipCreate`: boolean, default false
	* Only if VpkCreate = true
	* Creating ZIP archive with combinated .vpk